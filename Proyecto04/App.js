import 'react-native-gesture-handler';
import React,{Component} from 'react';
import {NavigationContainer} from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import Login from './app/components/Login/Login'
import {Text, View,Button,FlatList,TouchableOpacity,Image} from 'react-native'
import Viewnavigation from './app/components/Tavnavigator/navegacion'
import Detail from './app/components/Tavnavigator/Details';

const Stack= createStackNavigator();
function App() {
  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName="Login">
        <Stack.Screen name="Login" component={Login}/>
        <Stack.Screen name="navegacion" component={Viewnavigation}/>
        <Stack.Screen name="Detail" component={Detail}/>
      </Stack.Navigator>
    </NavigationContainer>
  );
}

export default App;
