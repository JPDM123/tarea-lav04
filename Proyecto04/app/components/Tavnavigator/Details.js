import React,{Component} from 'react';
import {StyleSheet,Text, View,Image} from 'react-native';


class Detail extends Component{
    constructor(props) {
        super(props);
        this.state = {};
      }
    render(){
        const {title,medium_cover_image,description_full} = this.props.route.params.item;
      return(
          <View>
              <View style={styles.text}>
                  <Text> {title} </Text>
              </View>
              <View style={{alignItems:'center', paddingVertical:20}}>
                <Image source={{uri:medium_cover_image}} style= {{height:450,width:400}}/>
              </View>
              <View style={{padding:20}}>
              <Text>{description_full}</Text>
              </View>
          </View>
      );
  }
}

export default Detail

const styles = StyleSheet.create({
    container:{
      flex: 1,
    },
    itemTextContainer:{
      flexDirection: 'column',
      paddingTop: 10,
    },
    text: {
      alignItems: 'center',
      padding: 10,
    },
    itemContainer: {
      flexDirection:'row',
      borderColor:'green',
    },
    countTex: {
      color : '#FF00FF',
    },
    img:{
      width: 150,
      height: 150,
      margin: 10,
      borderRadius: 10,
      borderColor: '#ff00ff00',
    },
    line:{
      height: 2, 
      width: '100%', 
      backgroundColor: '#DADADA',
    },
  });