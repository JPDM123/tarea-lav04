import React,{Component} from 'react';
import {StyleSheet,Text, View,FlatList,TouchableOpacity,Image, TabBarIOSItem} from 'react-native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs'
import ViewVideo from './ViewVideo'
import ViewSettings from './ViewSettings';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import { createMaterialBottomTabNavigator } from '@react-navigation/material-bottom-tabs';
//import { createMaterialBottomTabNavigator } from '@react-navigation/material-top-tabs' ;

//const Tab = createMaterialBottomTabNavigator()
const Tab = createMaterialBottomTabNavigator()
function Item(props){
  return(
    <View>
      <TouchableOpacity onPress={()=> {props.goDetails(props.item)}}>
      <View style={styles.itemContainer}>
            <Image source = {{uri:props.item.medium_cover_image}} style={styles.img}/>
            <View style={styles.itemTextContainer}>
              <Text style={styles.itemNameE}>{props.item.title}</Text>
              <Text>{props.item.year}</Text>
            </View>
        <View style={styles.line}/>
        </View>
        </TouchableOpacity>
    </View>
  )
}

class ConexionFet extends Component{
  constructor(props){
      super(props);
      this.state = {
          texValue:0,
          count: 0,
          items:[],
          error:null,
      };
  }
  goDetails=(item)=>{
      this.props.navigation.navigate('Detail',{
        item:item,
      });
    }
  async componentDidMount() {
      await fetch('https://yts.mx/api/v2/list_movies.json')
          .then(res => res.json())
          .then(
              result => {
                  console.warn('result',result.data.movies);
                  this.setState({
                      items:result.data.movies,
                  });
              },
              error => {
                  this.setState({
                      error: error,
                  });
              },
          );
  }
  render(){
   return (
      <View style={styles.container}>
          <FlatList
              data={this.state.items.length > 0 ? this.state.items : []}
              renderItem={({item}) => (
                  <Item item={item} goDetails={this.goDetails}/>
              )}
          /> 
      </View>
  );
  }
}

  class Viewnavigation extends Component {
    constructor(props) {
      super(props);
      this.state = {}; 
    }
    
    render(){
      return (
          <Tab.Navigator initialRouteName="Home" TabBarOptions={{
            activeTintColor:'#e91e63',
          }}>
            <Tab.Screen name="Home" component={ConexionFet} 
              options={{
              tabBarLabel:'Home', 
              tabBarIcon:({color,size}) => (
              <Icon name="home" color={color} size={size}/>
            ),
            }}/>
            <Tab.Screen name="ViewVideo" component={ViewVideo}
              options={{
              tabBarLabel:'Video', 
              tabBarIcon:({color,size}) => (
              <Icon name="bell" color={color} size={size}></Icon>
            ),
            }}/>
            <Tab.Screen name="ViewSettings" component={ViewSettings}
              options={{
              tabBarLabel:'Settings', 
              tabBarIcon:({color,size}) => (
              <Icon name="home" color={color} size={size}/>
            ),
            }}/>
          </Tab.Navigator>  
      );
    } 
}

export default Viewnavigation;

const styles = StyleSheet.create({
  container:{
    flex: 1,
  },
  itemTextContainer:{
    flexDirection: 'column',
    paddingTop: 10,
  },
  text: {
    alignItems: 'center',
    padding: 10,
  },
  itemContainer: {
    flexDirection:'row',
    borderColor:'green',
  },
  countTex: {
    color : '#FF00FF',
  },
  img:{
    width: 150,
    height: 150,
    margin: 10,
    borderRadius: 10,
    borderColor: '#ff00ff00',
  },
  line:{
    height: 2, 
    width: '100%', 
    backgroundColor: '#DADADA',
  },
});