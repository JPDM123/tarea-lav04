import React, {Component} from 'react';
import {View, Text, Image,StyleSheet,TextInput,FlatList, Appearance,TouchableOpacity} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';


function Item(props){
    return(
      <View>
        <TouchableOpacity>
        <View>
              <View style={styles.itemTextContainer}>
              <Icon.Button name={props.item.icono} backgroundColor="#3b5998">
              </Icon.Button>
                <Text style={{fontSize:25, padding:25}}>{props.item.opcion}</Text>
              </View>
          <View style={styles.line}/>
          </View>
          </TouchableOpacity>
      </View>
    )
  }
class ViewSettings extends Component {
    constructor(props) {
      super(props);
      this.state = {
        DATA : [
            {
                opcion:"Account",
                icono:"account-circle"
            },
            {
                opcion:"notifications",
                icono:"notifications"
            },
            {
                opcion:"Apperance",
                icono:"visibility"
            },
            {
                opcion:"Privacy & Security",
                icono:"security"
            },
            {
                opcion:"Help and suport",
                icono:"headset"
            },
            {
                opcion:"About",
                icono:"help-outline"
            },
        
        ]
      };
    }
    render() {
      return (
        <View  style={{flex: 1,alignItems:'center'}}>
            <View>
                <Text style={styles.title}>
                    Settings
                </Text>
            </View>
            <View>
            <TextInput 
                style= {{width: 400, borderColor: 'gray', borderWidth: 1}}
                placeholder={'Search for a settings'}
            />
            <FlatList
              data={this.state.DATA}
              renderItem={({item}) => (
                  <Item item={item}/>
              )}
          /> 
            </View>
        </View>
      );
    }
  }
  
  export default ViewSettings;
  
  var styles = StyleSheet.create({
      Containertitle:{
        fontSize:20,
      },
      title:{
        fontSize:20,
        padding:30
      },
      itemTextContainer:{
        flexDirection: 'row',
        paddingTop: 10,
      },
      line:{
        height: 2, 
        width: '100%', 
        backgroundColor: '#DADADA',
      },
    });