import React, {Component} from 'react';
import {View, Text, Image,StyleSheet} from 'react-native';
import Video from 'react-native-video';

class ViewVideo extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }
  render() {
    return (
      <View  style={{flex: 1,alignItems:'center',padding:40}}>
        <Text style={{fontSize: 40}}>Video</Text>
        <Video source={{uri: 'https://www.radiantmediaplayer.com/media/bbb-360p.mp4'}}   // Can be a URL or a local file.
            ref={(ref) => {
            this.player = ref}}                                                     
            style={styles.backgroundVideo}
            rate={1.0}
            volume={10}
            muted={false}
            controls
            resizeMode="cover"/>
        </View>
    );
  }
}

export default ViewVideo;

var styles = StyleSheet.create({
    backgroundVideo: {
        position: 'absolute',
        top: 150,
        left: 10,
        bottom: 150,
        right: 10,
    },
  });
